# Introduction

The only goal of this project is illustrating some issues detected using OpenAPI `$ref`erences in GitLab.

Don't hesitate asking for access if you want to play around with it a bit.
